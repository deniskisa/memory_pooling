
#include "pooling.h"
#include "UnitTest.h"
#include "ExampleClasses.h"
#include <assert.h>

// testing typedef char ByteType;
void TestDataTypeChar()
{
    Pool<ByteType>* p = CreatePool<ByteType>();


    ByteType* var1 = (ByteType*) AllocObjFromPool(p);
    *var1 = 'a';
    ByteType* var2 = (ByteType*) AllocObjFromPool(p);
    *var2 = 'b';
    ByteType* var3 = (ByteType*) AllocObjFromPool(p);
    *var3 = 'c';
    ByteType* var4 = (ByteType*) AllocObjFromPool(p);
    *var4 = 'd';
    ByteType* var5 = (ByteType*) AllocObjFromPool(p);
    *var5 = 'e';

    DeallocObjFromPool(p, var1);
    DeallocObjFromPool(p, var2);
    DeallocObjFromPool(p, var3);


    for (int i = 6; i<15; i++)
    {
      var1 =  (ByteType*) AllocObjFromPool(p);
      if (var1 != NULL)
      {
        *var1 = 'a'+i;
      }

    }


   DestroyPool<ByteType>(p);
   printf("Unit test for typedef ByteType PASS\n");
}


// testing typedef void* PointerType;
void TestDataTypePointerType()
{
    Pool<PointerType>* p = CreatePool<PointerType>();

    PointerType* var1, var2;
    for (int i = 0; i<g_MaxNumberOfObjectsInPool; i++)
     var1 = (PointerType*) AllocObjFromPool(p);

    assert(var1 != NULL);
    var2 = var1;

    var1 = (PointerType*) AllocObjFromPool(p);
    assert(var1 == NULL);

    DeallocObjFromPool(p, var2);
    var1 = (PointerType*) AllocObjFromPool(p);
    assert(var1 != NULL);

    var1 = (PointerType*) AllocObjFromPool(p);
    assert(var1 == NULL);


   DestroyPool<PointerType>(p);
   printf("Unit test for typedef PointerType PASS\n");
}



// testing datatype FixedStringType[256];
void TestDataTypeFixedString()
{
    Pool<FixedStringType[256]>* p = CreatePool<FixedStringType[256]>();

    FixedStringType* var1;
    FixedStringType* var2;
    for (int i = 0; i<g_MaxNumberOfObjectsInPool; i++)
     var1 = (FixedStringType*) AllocObjFromPool(p);

    assert(var1 != NULL);
    var2 = var1;

    var1 = (FixedStringType*) AllocObjFromPool(p);
    assert(var1 == NULL);

    DeallocObjFromPool(p, var2);
    var1 = (FixedStringType*) AllocObjFromPool(p);
    assert(var1 != NULL);

    var1 = (FixedStringType*) AllocObjFromPool(p);
    assert(var1 == NULL);


   DestroyPool<FixedStringType[256]>(p);
   printf("Unit test for datatype FixedStringType[256] PASS\n");
}


// testing struct Point;
void TestDataTypeStruct()
{
    Pool<struct Point>* p = CreatePool<struct Point>();

    struct Point* var1;
    struct Point* var2;
    for (int i = 0; i<g_MaxNumberOfObjectsInPool; i++)
     var1 = (struct Point*) AllocObjFromPool(p);

    assert(var1 != NULL);
    var2 = var1;

    var1 = (struct Point*) AllocObjFromPool(p);
    assert(var1 == NULL);

    DeallocObjFromPool(p, var2);
    var1 = (struct Point*) AllocObjFromPool(p);
    assert(var1 != NULL);

    var1 = (struct Point*) AllocObjFromPool(p);
    assert(var1 == NULL);


   DestroyPool<struct Point>(p);
   printf("Unit test for typedef struct Point PASS\n");
}


// testing class Base1;
void TestDataTypeClassBase1()
{
    Pool<Base1>* p = CreatePool<Base1>();

    Base1* var1;
    Base1* var2;
    for (int i = 0; i<g_MaxNumberOfObjectsInPool; i++)
     var1 = (Base1*) AllocObjFromPool(p);

    assert(var1 != NULL);
    var2 = var1;

    var1 = (Base1*) AllocObjFromPool(p);
    assert(var1 == NULL);

    DeallocObjFromPool(p, var2);
    var1 = (Base1*) AllocObjFromPool(p);
    assert(var1 != NULL);

    var1 = (Base1*) AllocObjFromPool(p);
    assert(var1 == NULL);


   DestroyPool<Base1>(p);
   printf("Unit test for typedef class Base1 PASS\n");
}

// testing class Base2;
void TestDataTypeClassBase2()
{
    Pool<Base2>* p = CreatePool<Base2>();

    Base2* var1;
    Base2* var2;
    for (int i = 0; i<g_MaxNumberOfObjectsInPool; i++)
     var1 = (Base2*) AllocObjFromPool(p);

    assert(var1 != NULL);
    var2 = var1;

    var1 = (Base2*) AllocObjFromPool(p);
    assert(var1 == NULL);

    DeallocObjFromPool(p, var2);
    var1 = (Base2*) AllocObjFromPool(p);
    assert(var1 != NULL);

    var1 = (Base2*) AllocObjFromPool(p);
    assert(var1 == NULL);


   DestroyPool<Base2>(p);
   printf("Unit test for typedef class Base2 PASS\n");
}


// testing class Derived;
void TestDataTypeClassDerived()
{
    Pool<Derived>* p = CreatePool<Derived>();

    Derived* var1;
    Derived* var2;
    for (int i = 0; i<g_MaxNumberOfObjectsInPool; i++)
     var1 = (Derived*) AllocObjFromPool(p);

    assert(var1 != NULL);
    var2 = var1;

    var1 = (Derived*) AllocObjFromPool(p);
    assert(var1 == NULL);

    DeallocObjFromPool(p, var2);
    var1 = (Derived*) AllocObjFromPool(p);
    assert(var1 != NULL);

    var1 = (Derived*) AllocObjFromPool(p);
    assert(var1 == NULL);


   DestroyPool<Derived>(p);
   printf("Unit test for typedef class Derived PASS\n");
}

// testing class NoDefaultConstructor;
void TestDataTypeClassNoDefaultConstr()
{
    Pool<NoDefaultConstructor>* p = CreatePool<NoDefaultConstructor>();

    NoDefaultConstructor* var1;
    NoDefaultConstructor* var2;
    for (int i = 0; i<g_MaxNumberOfObjectsInPool; i++)
     var1 = (NoDefaultConstructor*) AllocObjFromPool(p);

    assert(var1 != NULL);
    var2 = var1;

    var1 = (NoDefaultConstructor*) AllocObjFromPool(p);
    assert(var1 == NULL);

    DeallocObjFromPool(p, var2);
    var1 = (NoDefaultConstructor*) AllocObjFromPool(p);
    assert(var1 != NULL);

    var1 = (NoDefaultConstructor*) AllocObjFromPool(p);
    assert(var1 == NULL);


   DestroyPool<NoDefaultConstructor>(p);
   printf("Unit test for typedef class NoDefaultConstructor PASS\n");
}
