#ifndef POOLING_H_INCLUDED
#define POOLING_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>

const int g_MaxNumberOfObjectsInPool = 1000;

template <typename T>
struct ObjectInPool{
  T s;
  struct ObjectInPool* next_empty;
};

template <typename T>
struct Pool{
   struct ObjectInPool<T> pool_objects[g_MaxNumberOfObjectsInPool];
   struct ObjectInPool<T>* next_empty;
   struct ObjectInPool<T>* last_empty;
   int elem_count;                       // debug variable
   int size;                             // debug variable
};


// user API
template <typename T>
struct Pool<T>* CreatePool();

template <typename T>
void DestroyPool(struct Pool<T>* pool);

template <typename T>
void* AllocObjFromPool(struct Pool<T>* pool);

template <typename T>
void DeallocObjFromPool(struct Pool<T>* pool, void* object_to_dealloc);


// Auxiliary functions
template <typename T>
void InitPool(struct Pool<T>* pool);


// Implementation
// (is contained in the header due to it's template nature)
template <typename T>
void InitPool(struct Pool<T>* pool)
{
  pool->elem_count=0;
  pool->size = g_MaxNumberOfObjectsInPool;
  pool->next_empty = &pool->pool_objects[0];
  pool->last_empty = &pool->pool_objects[g_MaxNumberOfObjectsInPool-1];

  for (int i = 0; i<g_MaxNumberOfObjectsInPool-1; i++)
    pool->pool_objects[i].next_empty = &pool->pool_objects[i+1];

  pool->pool_objects[g_MaxNumberOfObjectsInPool-1].next_empty = NULL;
}

template <typename T>
struct Pool<T>* CreatePool()
{
    struct Pool<T>* res = (struct Pool<T>*) malloc(sizeof(struct Pool<T>));

    InitPool(res);

    return res;
}

template <typename T>
void DestroyPool(struct Pool<T>* pool) {free(pool);}

template <typename T>
void* AllocObjFromPool(struct Pool<T>* pool)
{
   struct ObjectInPool<T>* res =  pool->next_empty;
   if (res != NULL)
   {
     pool->next_empty = pool->next_empty->next_empty;
     pool->elem_count++;
   }

   return res;
}

// TODO  handling the case of deallocating the same object for several times
template <typename T>
void DeallocObjFromPool(struct Pool<T>* pool, void* object_to_dealloc)
{
  if (object_to_dealloc == NULL)
    return;

  pool->last_empty->next_empty = (struct ObjectInPool<T>*) object_to_dealloc;
  pool->last_empty = (struct ObjectInPool<T>*) object_to_dealloc;
  pool->last_empty->next_empty = NULL;

  if (pool->next_empty == NULL) // in case pool was already exhausted before this dealloc operation
    pool->next_empty = (struct ObjectInPool<T>*) object_to_dealloc;

  if (pool->elem_count>0)
    pool->elem_count--;
}

#endif // POOLING_H_INCLUDED
