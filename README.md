# README #

Memory pooling algorithm based on linked lists


### Intuition ###

* Main idea behind this memory pool implementation is to maintain a linked list of empty memory cells
* At each point in time there 2 pointers maintained inside a top-level structure: link to the next empty element and link to the last empty element
* Maintaining next empty element will allow to allocate fast and achieve O(1)
* Maintaining last empty element will allow to deallocate fast and achieve O(1)
* Such a design comes with 2 costs behind:
* 1) one-shot init activity needed to be performed in scope CreatePool() to link all empty memory cells
* 2) extra 4 bytes pointer at each element to point to the next empty element

### What is included ###

* pooling.h: memory pooling algorithm
* UnitTest.h, UnitTest.cpp: unit tests covering all example data types provided
* main.c: main routine that runs unit tests
* ExampleClasses.h: example datatypes provided

### Requirements fulfilled ###

* nearly achieved the desired time & space complexity, 
* reusable template-based implementation
* unit tests covering all provided data types
* compact, under ~100 LOC
* Tried to adhere C++ Google code style guidelines

### Time & Space complexity ###

* AllocObjFromPool()   - O(1) time
* DeallocObjFromPool() - O(1) time
* CreatePool()  - O(maximumNumberOfObjectsInPool) time.
* DestroyPool() - O(1) time
* Space complexity: maximumNumberOfObjectsInPool * (sizeof(Object) + 4bytes)

### Known Limitations ###

* Pooling behaves in the same fashion as malloc and does not call constructor or destructor methods. These shall be called by user separately
* Handling the case of deallocating the same object for several times is not yet covered
* No 100% code coverage
* No static code style (MISRA) was performed

### Honor code ###
I, Denys Kinshakov, hereby state that I have completed the following implementation entirely by myself without
 relying upon any third-party help nor on any internet based resources. This idea is purely my own inspiration.
