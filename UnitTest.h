#ifndef UNITTEST_H_INCLUDED
#define UNITTEST_H_INCLUDED




void TestDataTypeChar();
void TestDataTypePointerType();
void TestDataTypeFixedString();
void TestDataTypeStruct();
void TestDataTypeClassBase1();
void TestDataTypeClassBase2();
void TestDataTypeClassDerived();
void TestDataTypeClassNoDefaultConstr();

#endif // UNITTEST_H_INCLUDED
