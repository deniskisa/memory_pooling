#include <stdio.h>
#include <stdlib.h>

#include "UnitTest.h"

int main()
{
    TestDataTypeChar(); // testing typedef char ByteType
    TestDataTypePointerType(); // testing typedef void* PointerType
    TestDataTypeFixedString(); // testing fixed string
    TestDataTypeStruct();   // testing struct Point
    TestDataTypeClassBase1(); // testing class Base1
    TestDataTypeClassBase2(); // testing class Base2
    TestDataTypeClassDerived(); // testing class Derived
    TestDataTypeClassNoDefaultConstr(); // testing class NoDefaultConstr

    return 0;
}
